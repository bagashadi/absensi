export default function MidUser( to, from, next ){
    console.log("test log", to, from);
    var token = parseJwt(localStorage.getItem('token'));
    if(!localStorage.getItem('token')){
        return next({path: '/auth'});
    }
    if(Date.now() / 1000 > token.exp){
        localStorage.removeItem('token');
        return next({path: '/auth'});
    }
    function parseJwt (token) {
        var base64Url = token.split('.')[1];
        var base64 = decodeURIComponent(atob(base64Url).split('').map(function(c) {
            return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
        }).join(''));

        return JSON.parse(base64);
    };
    return next();
}