export default function logout( to, from, next ){
    console.log("test log", to, from);
    localStorage.removeItem('token');
    return next({path: '/auth'});
}