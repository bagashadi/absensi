import DashboardLayout from "@/pages/Layout/DashboardLayout.vue";

import Dashboard from "@/pages/Dashboard.vue";
import UserProfile from "@/pages/UserProfile.vue";
import TableList from "@/pages/TableList.vue";
import Typography from "@/pages/Typography.vue";
import Icons from "@/pages/Icons.vue";
import Maps from "@/pages/Maps.vue";
import Notifications from "@/pages/Notifications.vue";
import UpgradeToPRO from "@/pages/UpgradeToPRO.vue";
import EditUser from "@/pages/EditUser.vue";
import Absensi from "@/pages/Absensi.vue";
import LoginLayout from "@/pages/Layout/LoginLayout.vue";
import Login from "@/pages/Layout/Login.vue";

import MidDashboard from "../middleware/MidDashboard";
import log from "../middleware/log";
import logout from "../middleware/logout";
import MidAbsensi from "../middleware/MidAbsensi";
import MidUser from "../middleware/MidUser";
import MidEditUser from "../middleware/MidEditUser";

const routes = [
  {
    path: "/auth",
    component: LoginLayout,
    redirect: "/auth/login",
    children: [
      {
        path: "login",
        name: "Login",
        component: Login,
        beforeEnter: log
      },
      {
        path: "logout",
        name:"Logout",
        beforeEnter: logout
      }
    ]
  },
  {
    path: "/",
    component: DashboardLayout,
    redirect: "/dashboard",
    children: [
      {
        path: "dashboard",
        name: "Dashboard",
        component: Dashboard,
        beforeEnter: MidDashboard
      },
      {
        path: "user",
        name: "Master User",
        component: UserProfile,
        beforeEnter: MidUser
      },
      {
        path: "table",
        name: "Table List",
        component: TableList
      },
      {
        path: "typography",
        name: "Typography",
        component: Typography
      },
      {
        path: "icons",
        name: "Icons",
        component: Icons
      },
      {
        path: "maps",
        name: "Maps",
        meta: {
          hideFooter: true
        },
        component: Maps
      },
      {
        path: "notifications",
        name: "Notifications",
        component: Notifications
      },
      {
        path: "upgrade",
        name: "Upgrade to PRO",
        component: UpgradeToPRO
      },
      {
        path: "edit_user",
        name: "Edit User",
        component: EditUser,
        beforeEnter: MidEditUser
      },
      {
        path: "absensi",
        name: "Absensi",
        component: Absensi,
        beforeEnter: MidAbsensi
      }
    ]
  }
];

export default routes;
