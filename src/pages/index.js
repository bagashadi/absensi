// Cards
import UserCard from "../pages/UserProfile/UserCard.vue";

// Forms
import AddUserForm from "../pages/UserProfile/AddUserForm.vue";
import EditUserForm from "../pages/UserProfile/EditUserForm.vue";
import { cpus } from "os";

export { UserCard, AddUserForm, EditUserForm };